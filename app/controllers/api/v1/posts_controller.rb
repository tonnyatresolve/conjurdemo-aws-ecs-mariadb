class Api::V1::PostsController < ApplicationController
    before_action :verify_token
    def index
        @posts = Post.all
        render json: @posts
    end
    def create
        @post = Post.new(user_id: session[:user_id], description: params[:description])
        if @post.save
            render json: @post
        else
            render json: {error: 'process not completed'}
        end
    end
    def update
        @post = Post.where(id: params[:id], user_id: session[:user_id]).first
        if @post.update_attribute(:description, params[:description])
            render json: @post
        else
            render json: {error: 'process not completed'}
        end
    end
    def destroy
        @post = Post.where(id: params[:id], user_id: session[:user_id]).first
        if @post.destroy
            render json: {status: 'successful'}
        else
            render json: {error: 'process not completed'}
        end
    end
end
