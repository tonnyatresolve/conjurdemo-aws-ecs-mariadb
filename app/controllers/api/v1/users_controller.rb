class Api::V1::UsersController < ApplicationController
    def index
        @users = User.all
        render json: @users
    end
  
    def request_token
        @user = User.where('email = ? AND password = ?', params[:email], params[:password]).first
        if @user.present?
            render json: {token: @user.auth_token, info: 'Use your token to make api calls'}
        else
            render json: {error: 'User not Found'}
        end
    end
end
