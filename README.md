# README

To build and run the docker image.

* docker build -t rails-api:<version> .

* docker run -d -p 3000:3000 --env RAILS_DB_ADDR=<the docker host IP with MariaDB> <docker image in your registry>

+ summon -p /usr/local/lib/summon/summon-conjur docker run -d -p 3000:3000 --env RAILS_DB_ADDR=<the docker host IP with MariaDB> <docker image in your registry>